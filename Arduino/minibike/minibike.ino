

// Min around 300 Max around 800
int sensorPin = A0;    // select the input pin for the potentiometer
int pwmPin = 6;      // select the pin for the LED
int enablePin = 5;
int sensorValue = 0;  // variable to store the value coming from the sensor
int minimum = 212;
int range = 811;
int maximum = 1023;

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(pwmPin, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(sensorPin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  //readMinimum();
}

void loop() {



  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  if (sensorValue > minimum){
    digitalWrite(enablePin, HIGH);
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)  
    analogWrite(pwmPin, map(sensorValue, minimum, maximum, 0, 255));
  }
  else{
    digitalWrite(enablePin, LOW);
    digitalWrite(LED_BUILTIN, LOW);
  }
  delay(100);

}

int readValToWrite(int sensorVal){
  int returnVal = 255*((sensorVal-minimum)/range);
  if (returnVal > 255){
    returnVal = 255;
  }
  return returnVal;
}

void readMinimum(){
  int minimumTemp = 0;
  for (int i=0; i<10;i++){
    minimumTemp += analogRead(sensorPin);
    delay(50);
  }
  minimum = (minimumTemp/10) + 20;
  range = maximum-minimum;
}
